<?php include_once('_header.php') ?>
<div class="card">
    <div class="card-body">
        <form action="uploadManager.php" method="post" enctype="multipart/form-data">
            <h2>Subir Apuntes</h2>
            <select name="subject" class="form-control">
                <option value="mates">Mates</option>
                <option value="lengua">Lengua</option>
            </select>
            <label for="title">Nombre:</label>
            <input type="text" name="title" id="title" class="form-control">
            <label for="doc">Subir Documento (PDF):</label>
            <input type="file" name="doc" id="doc" class="form-control">
            <div class="col-auto mt-3">
                <button type="submit" class="btn btn-primary">Subir</button>
            </div>
        </form>
    </div>
</div>
<?php include_once('_footer.php') ?>