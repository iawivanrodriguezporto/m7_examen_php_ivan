<?php
define("MAX_SIZE", 6); //Max Mb
define('UPLOAD_FOLDER',"../assets/uploads");

class Upload
{
    private $file = null;
    private $title = null;
    private $error = null;
    private $filePath = null;

    function __construct($file, $title)
    {
        $this->file = $file;
        $this->title = $title;
        $this->uploadFile();
    }
    /*
    * 
    *   uploadFile()
    *   PARAMS: none
    */
    function uploadFile()
    {

        //Check if the folder exist is a valid picture and title
        //$_SERVER['DOCUMENT_ROOT'] return the folder of the server where is our APP
        if (!is_dir($_SERVER['DOCUMENT_ROOT'] . '/' . UPLOAD_FOLDER)) {
            if (!mkdir($_SERVER['DOCUMENT_ROOT'] . '/' . UPLOAD_FOLDER))
                throw new UploadError("Error: No se ha podido crear el directorio");
        } else {
            header('Location: index.php?upload=error&msg=' . urlencode("No se ha podido crear el directorio"));
        }
        if (!is_writable($_SERVER['DOCUMENT_ROOT'] . '/' . UPLOAD_FOLDER))
            throw new UploadError("Error: No puedes escribir en el directorio");
        else {
            header('Location: index.php?upload=error&msg=' . urlencode("No puedes escribir en el directorio"));
        }
        // Check if file was uploaded without errors
        if ($this->file["error"] != 0)
            throw new UploadError("Error: " . $this->file["error"]);
        else {
            header('Location: index.php?upload=error&msg=' . urlencode("Error al pujar el fitxer"));
        }
        //Define all data of the FILE
        $filename = $this->file["name"];
        $filesize = $this->file["size"];
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        $uploadPath = UPLOAD_FOLDER . $filename;

        // Verify file extension (PDF)
        /*
        *  BLOQUE A: VERIFICA QUE SE SUBE UN ARCHIVO CON EXTENSION PDF Y DE TIPO MIME PDF
        *
        */
        // 
        
        $fileExtension = explode($filename,".");
        if ($fileExtension[0] != "pdf" || $fileExtension[1] != "pdf") {
            throw new UploadError("Error : Incorrecta extensio o tipus d'arxiu");
        }

        // Verify file size - 5MB maximum
        $maxsize = MAX_SIZE * 1024 * 1024;
        if ($filesize > $maxsize)
            throw new UploadError("Error: File size is larger than the allowed limit.");

        // Check whether file exists before uploading it
        /*
        BLOQUE B: SUBE EL ARCHIVO A LA RUTA ABSOLUTA DE DESTINO. Si se ha subido correctamente se define el filePath
        */

        if (move_uploaded_file($_FILES["name"]["tmp"],$uploadPath)) {
            $this->filePath = $uploadPath;
        }

    }

    /*
    * Esta función añadirá al archivo de base de datos de la asignatura la información del archivo subido.  Si no existe
    * La base de datos se creará en assets/uploads
    * addUploadToFile
    * $subject: Asignatura (base de datos) donde se guardará la información del archivo
    */
    function addUploadToFile($subject)
    {
        $file = fopen($this->file,"a");
        while (!feof($file)) {
            $linea = $this->title . "###" .$this->filePath . '\n';
            fwrite($file,$linea);
        }
        fclose($file);
    }
    function getError()
    {
        return $this->error;
    }
    function getFile()
    {
        return $this->filePath;
    }
}
